<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 8/27/2015
  Time: 10:57 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Teacher's Registration</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <!-- Latest compiled and minified JavaScript -->
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li> <a href="index.jsp"><i class="fa fa-home">Home</i></a></li>
                <li><a href="StudentReg.jsp"><i class="fa fa-user">Student Registration</i></a></li>
                <li class="active"><a href="#"><i class="fa fa-user">Teacher's Registration</i></a></li>
                <li><a href="StudentLogin.jsp"><i class="fa fa-user">Student's Login</i></a></li>
                <li><a href="TeacherLogin.jsp"><i class="fa fa-user">Teacher's Login</i></a></li>

            </ul>
        </div>
    </div>
</nav>
    <h1>Register</h1>
    <form action="/TeacherReg" method="post">
        <div class="form-group">
        <label class="col-sm-2 control-label">Name:</label>
            <div class="col-sm-4">
            <input type="text" name="name" >
            </div>
        </div>
        <div class="form-group">
        <label class="col-sm-2 control-label">Username:</label>
            <div class="col-sm-4">
            <input type="text" name="username" >
            </div>
        </div>
        <div class="form-group">
        <label class="col-sm-2 control-label">Password:</label>
            <div class="col-sm-4">
            <input type="password" name="password" >
            </div>
        </div>
        <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <button type="submit" class="btn btn-primary btn-sm" value="Create">Create</button>
            </div>
        </div>
    </form>
</body>
</html>
