/**
 * Created by user on 9/18/2015.
 */

function pullOptions(option){
    this.option = option;
}

pullOptions.prototype.getOption = function(){
    return this.option;
}

pullOptions.prototype.setOption = function(option){
    this.option = option;
}

function allOptions(){
    this.options = new Array();
}

allOptions().prototype.allofoptions = function(option){
    this.options[option] = pullOptions(option);
}