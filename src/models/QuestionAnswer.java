package models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 8/27/2015.
 */
@DatabaseTable(tableName = "QuestionAnswer")
public class QuestionAnswer {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String questions;
    @DatabaseField
    private String optionA;
    @DatabaseField
    private String optionB;
    @DatabaseField
    private String optionC;
    @DatabaseField
    private String optionD;
    @DatabaseField
    private String answer;
    public QuestionAnswer(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestions() {
        return questions;
    }

    public void setQuestions(String questions) {
        this.questions = questions;
    }

    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    public String getOptionD() {
        return optionD;
    }

    public void setOptionD(String optionD) {
        this.optionD = optionD;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
