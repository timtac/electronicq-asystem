package models;



/**
 * Created by user on 9/15/2015.
 */
public class DateClass {
    private String area;
    private String option;

    public DateClass(){

    }
    public String getArea(){
        return area;
    }

    public void setArea (String area){
        this.area = area;
    }

    public String getOption(){
        return option;
    }

    public void setOption(String option){
        this.option = option;
    }
}
