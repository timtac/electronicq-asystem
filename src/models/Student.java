package models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 8/27/2015.
 */
@DatabaseTable(tableName = "")
public class Student {
    @DatabaseField(generatedId = true)
    private int student_id;
    @DatabaseField
    private String student_name;
    @DatabaseField
    private String username;
    @DatabaseField
    private String password;

    public Student(){

    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
