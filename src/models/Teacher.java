package models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 8/27/2015.
 */
@DatabaseTable
public class Teacher {
    @DatabaseField(generatedId = true)
    private int teacher_id;
    @DatabaseField
    private String teacher_name;
    @DatabaseField
    private String teacher_username;
    @DatabaseField
    private String teacher_password;

    public Teacher(){

    }

    public int getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getTeacher_username() {
        return teacher_username;
    }

    public void setTeacher_username(String teacher_username) {
        this.teacher_username = teacher_username;
    }

    public String getTeacher_password() {
        return teacher_password;
    }

    public void setTeacher_password(String teacher_password) {
        this.teacher_password = teacher_password;
    }
}
