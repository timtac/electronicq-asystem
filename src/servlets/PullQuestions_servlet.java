package servlets;
import com.google.gson.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


import dao.Question_dao;
import models.QuestionAnswer;


/**
 * Created by user on 9/1/2015.
 */
@WebServlet("/pull")
public class PullQuestions_servlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{

        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        try {
            Question_dao question_dao = new Question_dao();
            List<QuestionAnswer> list = question_dao.getQuestions();
            JsonArray arrayJs = new JsonArray();
            for(int count=0; count<list.size();count++) {

                QuestionAnswer obj = list.get(count);
                JsonObject jsonObject = new JsonObject();

                jsonObject.addProperty("questionId", obj.getId());
                jsonObject.addProperty("Question", obj.getQuestions());
                jsonObject.addProperty("optiona", obj.getOptionA());
                jsonObject.addProperty("optionb", obj.getOptionB());
                jsonObject.addProperty("optionc", obj.getOptionC());
                jsonObject.addProperty("optiond", obj.getOptionD());
                jsonObject.addProperty("answer", obj.getAnswer());

                arrayJs.add(jsonObject);
            }
            writer.println(arrayJs.toString());
        }
        catch(Exception ex){

        }
    }


   /* public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException{
        String ques = request.getParameter("area");
        String ans = request.getParameter("option");


        try{
            QuestionAnswer question = new QuestionAnswer();
            Question_dao dao = new Question_dao(question);

            if(dao.getAnswersByQues(ques,ans)){
                response.setContentType("text/html");
                PrintWriter writer = response.getWriter();
                writer.println("<html><body>");
                writer.println("Correct Answer!!!");
                writer.println("</body></html>");
                writer.close();
            }
            else{
                response.setContentType("text/html");
                PrintWriter writer = response.getWriter();
                writer.println("<html><body>");
                writer.println("Wrong Answer!!!");
                writer.println("</body></html>");
                writer.close();
            }
        }
        catch(Exception ex){
            response.setContentType("text/html");
            PrintWriter writer = response.getWriter();
            writer.println("<html><body>");
            writer.println("Error");
            writer.println("</body></html>");
            writer.close();
        }
    }*/
}
