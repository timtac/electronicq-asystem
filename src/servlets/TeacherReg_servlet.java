package servlets;

import dao.Teacher_dao;
import encrypt.CryptoUtil;
import models.Teacher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 8/27/2015.
 */
@WebServlet("/TeacherReg")
public class TeacherReg_servlet  extends HttpServlet{

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String name = request.getParameter("name");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();

        try{

            CryptoUtil cryptoUtil = new CryptoUtil();
            String key = "timi1234";
            String encPassword = cryptoUtil.encrypt(key,password);

            Teacher teacher = new Teacher();
            teacher.setTeacher_name(name);
            teacher.setTeacher_username(username);
            teacher.setTeacher_password(encPassword);

            new Teacher_dao(teacher).registerTeacher();
            session.setAttribute("username", username);
            session.setAttribute("login", "true");
            response.sendRedirect("/InputQuestion.jsp");
        }
        catch(Exception ex){
            response.setContentType("text/html");
            PrintWriter writer = response.getWriter();
            writer.println("<html><body>");
            writer.println("Error!!!");
            writer.println("</body></html>");
            writer.close();
        }
    }
}
