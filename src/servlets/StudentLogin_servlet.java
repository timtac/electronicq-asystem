package servlets;

import dao.Student_dao;
import encrypt.CryptoUtil;
import models.Student;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 8/27/2015.
 */
@WebServlet("/StudentLogin")
public class StudentLogin_servlet extends HttpServlet{

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            HttpSession session = request.getSession();

        if(session != null) {
            try {
                Student student = new Student();
                Student_dao busObj = new Student_dao(student);

                CryptoUtil cryptoUtil = new CryptoUtil();
                String key = "timi1234";
                String encPassword = cryptoUtil.encrypt(key, password);

                if (busObj.authenticateStudent(username, encPassword)) {

                    session.setAttribute("username",username);
                    session.setAttribute("loginstate","true");
                    response.sendRedirect("/QuestionAnswer.jsp");


                }
                else{
                    response.setContentType("text/html");
                    PrintWriter writer = response.getWriter();
                    writer.println("<html><body>");
                    writer.println("Invalid Username and password");
                    writer.println("</body></html>");
                    writer.close();

                }
            } catch (Exception ex) {
                response.setContentType("text/html");
                PrintWriter writer = response.getWriter();
                writer.println("<html><body>");
                writer.println("Error");
                writer.println("</body></html>");
                writer.close();
            }
        }
    }
}
