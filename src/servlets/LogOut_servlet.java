package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 9/11/2015.
 */
@WebServlet("/logout")
public class LogOut_servlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        HttpSession session = request.getSession();
        try {
            session.removeAttribute("username");
            session.invalidate();
            response.sendRedirect("/index.jsp");
        }
        catch (Exception ex){

        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException{
        String[] option =request.getParameterValues("option");
        String area = request.getParameter("area");

       /* for(int i =0;i<option.length; i++){
            System.out.println(option[i]);
        } */
            for(int i = 0;i < option.length; i++){
                response.setContentType("text/html");
                PrintWriter writer = response.getWriter();
                writer.println("<html><body>");
                writer.println("all options are" + option[i]);
                writer.println("</body></html>");
                writer.close();
                System.out.println(option[i]);
            }

    }
}
