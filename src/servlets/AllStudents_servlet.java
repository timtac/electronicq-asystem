package servlets;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dao.Student_dao;
import models.Student;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by user on 9/8/2015.
 */
@WebServlet("/allStudents")
public class AllStudents_servlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException{

        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        try{
            JsonArray jsonArray = new JsonArray();
            Student_dao student_dao = new Student_dao();
            List<Student> list = student_dao.allStudents();


            for(int count = 0; count < list.size(); count++){
                Student studObj = list.get(count);
                JsonObject jsonObject = new JsonObject();

                jsonObject.addProperty("ID",studObj.getStudent_id());
                jsonObject.addProperty("Name",studObj.getStudent_name());
                jsonObject.addProperty("Username", studObj.getUsername());

                jsonArray.add(jsonObject);
            }

            writer.println(jsonArray.toString());
        }
        catch(Exception ex){

        }
    }
}
