package servlets;

import dao.Student_dao;
import encrypt.CryptoUtil;
import models.Student;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 8/27/2015.
 */
@WebServlet("/StudentReg")
public class StudentReg_servlet extends HttpServlet{



    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
            String name =request.getParameter("name");
            String username = request.getParameter("username");
            String password = request.getParameter("password");

        HttpSession session = request.getSession();


        try {
            CryptoUtil cryptoUtil = new CryptoUtil();
            String key ="timi1234";
            String encPassword = cryptoUtil.encrypt(key,password);

            Student student = new Student();
            student.setStudent_name(name);
            student.setUsername(username);
            student.setPassword(encPassword);

            new Student_dao(student).registerStudent();
            session.setAttribute("username", username);
            session.setAttribute("loginstate","true");
            response.sendRedirect("/QuestionAnswer.jsp");
        }
        catch(Exception ex){
            response.setContentType("text/html");
            PrintWriter writer = response.getWriter();
            writer.println("<html><body>");
            writer.println("Error!!!");
            writer.println("</body></html>");
            writer.close();
        }
    }
}
