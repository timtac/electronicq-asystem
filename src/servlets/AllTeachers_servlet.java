package servlets;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dao.Teacher_dao;
import models.Teacher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by user on 9/8/2015.
 */
@WebServlet("/allTeachers")
public class AllTeachers_servlet extends HttpServlet{

    public void doGet( HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{

        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        try {
            JsonArray jsonArray = new JsonArray();
            Teacher_dao teacher_dao = new Teacher_dao();
            List<Teacher> list = teacher_dao.allTeachers();
            for(int count = 0;count < list.size(); count++){

                JsonObject jsonObject = new JsonObject();
                Teacher teaObj = list.get(count);

                jsonObject.addProperty("ID",teaObj.getTeacher_id());
                jsonObject.addProperty("Name",teaObj.getTeacher_name());
                jsonObject.addProperty("Username", teaObj.getTeacher_username());

                jsonArray.add(jsonObject);
            }
            writer.println(jsonArray.toString());
        }
        catch(Exception ex){

        }
    }
}
