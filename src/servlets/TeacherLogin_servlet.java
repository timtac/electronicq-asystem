package servlets;

import dao.Teacher_dao;
import encrypt.CryptoUtil;
import models.Teacher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 8/27/2015.
 */
@WebServlet("/TeacherLogin")
public class TeacherLogin_servlet  extends HttpServlet{

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();

        if(session != null) {
            try {
                Teacher teacher = new Teacher();
                Teacher_dao obj = new Teacher_dao(teacher);

                CryptoUtil cryptoUtil = new CryptoUtil();
                String key = "timi1234";
                String encPassword = cryptoUtil.encrypt(key, password);

                if (obj.authenticateTeacher(username, encPassword)) {
                        session.setAttribute("username",username);
                        session.setAttribute("login","true");
                    response.sendRedirect("/InputQuestion.jsp");
                }
            } catch (Exception ex) {
                response.setContentType("text/html");
                PrintWriter writer = response.getWriter();
                writer.println("<html><body>");
                writer.println("Error");
                writer.println("</body></html>");
                writer.close();
            }
        }
    }

}
