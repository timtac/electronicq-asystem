package servlets;

import dao.Question_dao;
import models.QuestionAnswer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 8/27/2015.
 */
@WebServlet("/Question")
public class Question_servlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
        String question = request.getParameter("question");
        String optionA = request.getParameter("optionA");
        String optionB = request.getParameter("optionB");
        String optionC = request.getParameter("optionC");
        String optionD = request.getParameter("optionD");
        String answer = request.getParameter("answer");

        try{
            QuestionAnswer questionAnswer = new QuestionAnswer();
            questionAnswer.setQuestions(question);
            questionAnswer.setOptionA(optionA);
            questionAnswer.setOptionB(optionB);
            questionAnswer.setOptionC(optionC);
            questionAnswer.setOptionD(optionD);
            questionAnswer.setAnswer(answer);

            new Question_dao(questionAnswer).setQuestion();
            response.setContentType("text/html");
            PrintWriter writer = response.getWriter();
            writer.println("<html><body>");
            writer.println("Done!!!");
            writer.println("</body></html>");
            writer.close();
        }
        catch(Exception ex){

        }
    }
}
