package context;

import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.table.TableUtils;
import dao.DbConstants;
import models.QuestionAnswer;
import models.Student;
import models.Teacher;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by user on 8/27/2015.
 */
public class DatabaseContext implements ServletContextListener {

    JdbcPooledConnectionSource con;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent){
        try{
            con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
            con.setUsername(DbConstants.DB_USER);
            con.setPassword(DbConstants.DB_PWD);

            TableUtils.createTableIfNotExists(con, QuestionAnswer.class);
            TableUtils.createTableIfNotExists(con, Student.class);
            TableUtils.createTableIfNotExists(con, Teacher.class);
        }
        catch(Exception ex){

        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent){

    }
}
