package context;

/**
 * Created by user on 9/14/2015.
 */
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;


@WebFilter(filterName="RequestLogger",urlPatterns = "/*")
public class MyRequestFilter implements Filter {
    private int count =0;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {


           // count = count + 1;
            //System.out.println("Request Log Number :" + count);
            //System.out.println(servletRequest.getLocalAddr());
            //System.out.println(servletRequest.getLocale());
            //System.out.println(servletRequest.getLocalName());
            //System.out.println(servletRequest.getProtocol());
            //System.out.println(servletRequest.getRemoteAddr());
            //System.out.println(servletRequest.getRemoteHost());
            //System.out.println(servletRequest.getRemotePort());
    try {
        filterChain.doFilter(servletRequest, servletResponse);
    }
    catch(Exception ex){
        ex.printStackTrace();
    }
    }

    @Override
    public void destroy() {

    }
}
