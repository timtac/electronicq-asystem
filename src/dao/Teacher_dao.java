package dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import models.Teacher;

import java.util.List;

/**
 * Created by user on 8/27/2015.
 */
public class Teacher_dao {
    private Teacher teacher;
    JdbcPooledConnectionSource con;

    public  Teacher_dao() throws  Exception{
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public Teacher_dao(Teacher teacher) throws Exception{
            this.teacher = teacher;
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public void registerTeacher() throws Exception{
        Dao<Teacher,String> teaDao = DaoManager.createDao(con,Teacher.class);
        teaDao.create(teacher);
    }

    public boolean authenticateTeacher(String username,String password) throws Exception{
        Dao<Teacher,String> teaDao = DaoManager.createDao(con,Teacher.class);
        List<Teacher> list = teaDao.queryForAll();
            for(int i = 0; i < list.size(); i++){
                teacher = list.get(i);
                if((teacher.getTeacher_username().equals(username) && teacher.getTeacher_password().equals(password))){
                    return true;
                }
            }
        return false;
    }

    public List<Teacher> allTeachers() throws Exception{
        Dao<Teacher,String> teaDao = DaoManager.createDao(con,Teacher.class);
        return teaDao.queryForAll();
    }
}
