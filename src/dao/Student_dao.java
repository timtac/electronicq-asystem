package dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import encrypt.CryptoUtil;
import models.Student;

import java.util.List;

/**
 * Created by user on 8/27/2015.
 */
public class Student_dao {
    JdbcPooledConnectionSource con;
    private Student student;

    public Student_dao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public Student_dao(Student student) throws Exception{
            this.student = student;
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public void registerStudent() throws Exception{
        Dao<Student,String> stdao = DaoManager.createDao(con, Student.class);
        stdao.create(student);
    }

    public boolean authenticateStudent(String username, String password) throws Exception{
        Dao<Student,String> stdoao = DaoManager.createDao(con, Student.class);
        List<Student> list = stdoao.queryForAll();
            for(int i=0; i<list.size(); i++) {
                student = list.get(i);
                    if ((student.getUsername().equals(username)) && (student.getPassword().equals(password))) {
                        return true;
                    }
            }
        return false;
    }

    public List<Student> allStudents() throws Exception{
        Dao<Student,String> stdao = DaoManager.createDao(con, Student.class);
        return stdao.queryForAll();

    }
}
