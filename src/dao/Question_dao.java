package dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import models.QuestionAnswer;

import java.util.List;

/**
 * Created by user on 8/27/2015.
 */
public class Question_dao {
    QuestionAnswer question;
    JdbcPooledConnectionSource con;

    public Question_dao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public Question_dao(QuestionAnswer question) throws Exception{
        this.question = question;

        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public void setQuestion() throws Exception{
        Dao<QuestionAnswer,String> quesDao = DaoManager.createDao(con,QuestionAnswer.class);
        quesDao.create(question);
    }

    public List<QuestionAnswer> getQuestions() throws Exception{
        Dao<QuestionAnswer,String> quesDao = DaoManager.createDao(con, QuestionAnswer.class);
        return quesDao.queryForAll();
    }

    public QuestionAnswer getQuestionById(int id)throws Exception{
        Dao<QuestionAnswer,String> quesDao = DaoManager.createDao(con, QuestionAnswer.class);
        return quesDao.queryForId(String.valueOf(id));
    }

    public boolean getAnswersByQues(String ques,String ans) throws Exception{
        Dao<QuestionAnswer,String> quesDao = DaoManager.createDao(con,QuestionAnswer.class);
        List<QuestionAnswer> list = quesDao.queryForAll();
        for(int i=0;i<list.size();i++){
            question = list.get(i);
            if((question.getQuestions().equals(ques)) &&(question.getAnswer().equals(ans))){
                return true;
            }
        }
        return false;
    }

}
