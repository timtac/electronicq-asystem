<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 8/27/2015
  Time: 10:57 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"  %>
<%@ page import="dao.Question_dao"%>
<%@ page import="models.QuestionAnswer" %>
<%@ page import="models.Student" %>
<%--<script src="/js/jquery.jmpopups-0.5.1.js" type="text/javascript"></script>--%>

<%
    //confirm authentication
      if(session!=null) {
          String state = (String) session.getAttribute("loginstate");
          if (state == null) {
              response.sendRedirect("/StudentLogin.jsp");
          }
      }
    else{ response.sendRedirect("/StudentLogin.jsp");}


%>
<html>
<head>
    <title>Electronic Questions System</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <!-- Latest compiled and minified JavaScript -->
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li> <a href="index.jsp"><i class="fa fa-home">Home</i></a></li>
                <li><a href="StudentReg.jsp"><i class="fa fa-user">Student Registration</i></a></li>
                <li><a href="TeacherReg.jsp"><i class="fa fa-user">Teacher's Registration</i></a></li>
                <li class="active"><a href="StudentLogin.jsp"><i class="fa fa-user">Student's Login</i></a></li>
                <li><a href="TeacherLogin.jsp"><i class="fa fa-user">Teacher's Login</i></a></li>

            </ul>
        </div>
    </div>
</nav>

    <h1>Questions</h1>
    <div class="panel panel-success">
       <div class="panel-heading">
           <h3 class="panel-title">Prove Your Worth!</h3>
        </div>

        <div class="panel-body">
<%
    //Getting the first question and options by id.
     Question_dao model = new Question_dao();
     QuestionAnswer buzzObj =model.getQuestionById(1);
%>
            <span id="quesId" name="id"><%=buzzObj.getId()%></span>


            <form  class="form-horizontal" role="form" action="/pull" method="post" name="myForm">
                <div class="form-group">
                <label>Question:</label><br/>
                    <textarea class="form-control-static" id="ques" contenteditable="false" cols="100" rows="10" name="area" ><%=buzzObj.getQuestions()%></textarea>
                </div>

                <div class="form-group">
                    <div class="radio">
                        <label class="checkbox-inline" >
                            <input type="radio" name="option"  checked  value="A"><span id="option1" name="span"><%=buzzObj.getOptionA()%></span></input>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="radio">
                        <label >
                            <input type="radio"  name="option"  value="B"><span id="option2" name="span"><%=buzzObj.getOptionB()%></span></input>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="radio">
                        <label >
                            <input type="radio" name="option" value="C"><span id="option3" name="span"><%=buzzObj.getOptionC()%></span></input>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="radio">
                        <label>
                            <input type="radio" name="option" value="D" ><span id="option4" name="span"><%=buzzObj.getOptionD()%></span></input>
                        </label>
                    </div>
                </div>

                <div class="btn-toolbar">
                    <input type="submit" value="Submit" class="btn btn-sm btn-primary " id="submit">
                    <button type="button" class="btn btn-sm btn-primary" id="prev" onclick="">Prev</button>
                    <button type="button" class="btn btn-sm btn-success" id="next" onclick="">Next</button>
                </div>

            </form>
            <div>
            </div>
        </div>

    </div>

    <script src="/js/jquery.min.js"></script>
    <script>


        var initialstate = 1;
        var get_value = new Array();

        //next Button event handler
        $('#next').click(function(){
            //get radio values
            $(':radio').change(function(){

                $(':radio:checked').each(function () {
                    get_value.push($(this).val());
                });

            });
            $.ajax({
                type:'GET',
                url: '/pull',
                success: function (response) {

                    var jsonResponse = JSON.parse(response);
                   // var answer = jsonResponse[initialstate].answer;

                    //gets json objects from the server to form elements
                    if (jsonResponse.length > initialstate) {
                        initialstate = initialstate + 1;
                        var questionIde = jsonResponse[initialstate].questionId;
                        var rcdquestn = jsonResponse[initialstate].Question;
                        var optionA = jsonResponse[initialstate].optiona;
                        var optionB = jsonResponse[initialstate].optionb;
                        var optionC = jsonResponse[initialstate].optionc;
                        var optionD = jsonResponse[initialstate].optiond;
                        //replaces values in the form elements
                        $('#quesId').html(questionIde);
                        $('#ques').val(rcdquestn);
                        $('#option1').html(optionA);
                        $('#option2').html(optionB);
                        $('#option3').html(optionC);
                        $('#option4').html(optionD);

                    }
                    else if(initialstate == jsonResponse.length) {

                        alert('End of Questions, pls click submit!!');
                    }
                }
                ,
                error: function (response) {

                }

            });
        });

        initialstate--;
        //Prev Button
        $('#prev').click(function(){

            $.ajax({
                type:'GET',
                url: '/pull',
                success: function (response) {

                    var jsonResponse= JSON.parse(response);

                    if(initialstate > 0) {
                        initialstate = initialstate - 1;
                        var questionIde = jsonResponse[initialstate].questionId;
                        var rcdquestn = jsonResponse[initialstate].Question;
                        var optionA = jsonResponse[initialstate].optiona;
                        var optionB = jsonResponse[initialstate].optionb;
                        var optionC = jsonResponse[initialstate].optionc;
                        var optionD = jsonResponse[initialstate].optiond;

                        $('#quesId').html(questionIde);
                        $('#ques').val(rcdquestn);
                        $('#option1').html(optionA);
                        $('#option2').html(optionB);
                        $('#option3').html(optionC);
                        $('#option4').html(optionD);

                    }
                    else{
                        alert('dead end');
                    }
                },
                error: function (response) {

                }
            });
        });

        //submit Button
         $('#submit').click(function() {
             alert(get_value.join(' '));

        });
    </script>
<!--footer-->
    <footer>
        <hr>
            <%
               String username =(String) session.getAttribute("username");
            %>
        You are logged in as <%=username %><br /><a type="" href="/logout"> Logout</a>
    </footer>
<!--footer-->
</body>
</html>
