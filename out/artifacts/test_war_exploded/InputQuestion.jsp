<%@ page import="models.Teacher" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 8/27/2015
  Time: 1:28 PM
  To change this template use File | Settings | File Templates.
--%>
<%
    if(session != null) {
        String state = (String) session.getAttribute("login");
        if (state == null) {
            response.sendRedirect("/TeacherLogin.jsp");
        }
    }

%>
<html>
<head>
    <title>Input Questions</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <!-- Latest compiled and minified JavaScript -->
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li > <a href="index.jsp"><i class="fa fa-home">Home</i></a></li>
                <li><a href="StudentReg.jsp"><i class="fa fa-user">Student Registration</i></a></li>
                <li><a href="TeacherReg.jsp"><i class="fa fa-user">Teacher's Registration</i></a></li>
                <li><a href="StudentLogin.jsp"><i class="fa fa-user">Student's Login</i></a></li>
                <li class="active"><a href="TeacherLogin.jsp"><i class="fa fa-user">Teacher's Login</i></a></li>

            </ul>
        </div>
    </div>
</nav>
    <h1>Input Questions and answers</h1>

    <form action="/Question" method="post">
        <label>Question:</label><br/><textarea contenteditable="true" name="question" cols="100" rows="10"></textarea><br /><br />
        <label>A.</label><input type="text" name="optionA" maxlength="30"><br /><br />
        <label>B.</label><input type="text" name="optionB" maxlength="30"><br /><br />
        <label>C.</label><input type="text" name="optionC" maxlength="30"><br /><br />
        <label>D.</label><input type="text" name="optionD" maxlength="30"><br /><br />
        <label>Answer.</label><input type="text" name="answer" maxlength="30" placeholder="e.g-A B.."><br /><br />
        <p><input type="submit" value="Submit"><input type="reset" value="Reset" ></p>
    </form>

<script src="/js/jquery.min.js"></script>
<script type="text/javascript">
    function reset(){

    }
</script>

<footer>
<hr>
<%
   String username = (String) session.getAttribute("username");
%>
You are logged in as <%= username %> <a type=""  href="/logout" text-underline="none" alignment="right" > Logout</a>  Copyright&copy; &circledS; &hearts; Official&nbsp;Reviews&nbsp;Timtac&nbsp; <br/>
<%@ include file="WEB-INF/footer.jspf"%>
</footer>
</body>
</html>
