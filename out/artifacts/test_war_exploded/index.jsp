<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 8/27/2015
  Time: 8:34 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Electronic Questions System</title>
      <link rel="stylesheet" href="/css/bootstrap.min.css">

      <!-- Optional theme -->
      <link rel="stylesheet" href="fonts/css/font-awesome.min.css">
      <link rel="stylesheet" href="css/animate.min.css">
      <link rel="stylesheet" href="css/custom.css">
      <!-- Latest compiled and minified JavaScript -->
  </head>
  <body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
           <li class="active"><a href="#"><i class="fa fa-home">Home</i></a></li>
            <li><a href="StudentReg.jsp"><i class="fa fa-user">Student Registration</i></a></li>
            <li><a href="TeacherReg.jsp"><i class="fa fa-user">Teacher's Registration</i></a></li>
            <li><a href="StudentLogin.jsp"><i class="fa fa-user">Student's Login</i></a></li>
            <li><a href="TeacherLogin.jsp"><i class="fa fa-user">Teacher's Login</i></a></li>

        </ul>
            </div>
        </div>
    </nav>

  </body>
</html>
